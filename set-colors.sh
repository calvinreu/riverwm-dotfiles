#!/bin/fish

#copy dunst colorsceme
cp ~/.config/colors/dunst/$argv[1].conf ~/.config/dunst/dunstrc

#copy alacritty colorsceme
cp ~/.config/colors/alacritty.yml ~/.config/alacritty/colors.yml
#replace colors: *unset with colors: *$argv[1] in alacritty config file
sed -i 's/unset/'$argv[1]'/g' ~/.config/alacritty/colors.yml

#copy fish catppuccin colorsceme
cp ~/.config/colors/catppuccin.fish ~/.config/fish/conf.d/catppuccin.fish
#replace unset with $argv[1] in fish config file
sed -i 's/unset/'$argv[1]'/g' ~/.config/fish/conf.d/catppuccin.fish

#copy wofi/$argv[1].css colorsceme
cp ~/.config/colors/wofi/$argv[1].css ~/.config/wofi/style.css