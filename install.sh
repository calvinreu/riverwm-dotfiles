#! /bin/fish

set SCRIPT_DIR (cd (dirname (status -f)); and pwd)
git pull

cd $SCRIPT_DIR

# copy config files
./copy_config.sh

#read colorsceme
echo "Enter your color scheme name:"
set COLOR_SCHEME (read)
#set colorsceme
./set-colors.sh $COLOR_SCHEME

#install rofi theme
cd tmp
git clone https://github.com/catppuccin/rofi && cd rofi/deathemonic
cp -r * ~/.config/rofi