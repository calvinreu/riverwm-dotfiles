#! /bin/fish
#install config files and backup old config files


#check if folder to install is given
if test -z $1
    # use default install folder 
    set INSTALL_FOLDERS (ls config/)
else
    # use $1 as install folder
    set INSTALL_FOLDERS (ls "$1/")
end

#backup names by date
set BACKUP_FOLDER $HOME/.backup/(date)/
mkdir -p $BACKUP_FOLDER

#numbering backups
#set BACKUP_FOLDER "../backup/"(count (ls ../backup))"/"

for folder in $INSTALL_FOLDERS;
    mv  $HOME/.config/$folder $BACKUP_FOLDER;
    cp -r ./config/$folder $HOME/.config;
end