#!/bin/fish

## MOD
set MOD "Alt"

# switch to and from command mode
riverctl map normal MOD + M spawn "~/.config/scripts/command_mode_start.fish"
riverctl map command None spawn "~/.config/scripts/command_mode_end.fish"

### NORMAL MODE ###

# $MOD+Q to close the focused view riverctl map normal $MOD Q close
riverctl map normal $MOD Q close

# launch terminal
riverctl map normal $MOD Return spawn "alacritty"

# launch menu and quit menu
riverctl map normal None $MENU spawn "~/.config/scripts/menu.fish"

# $MOD+Shift+E to exit river
riverctl map normal $MOD+Shift E exit

# switch focus
riverctl map normal $MOD TAB swap next



### workspaces/tags ###
for i in (seq 0 9);
    set tags (math 2 ^ (math $i))

    # $MOD+[0-9] to focus tag [0-9]
    riverctl map normal $MOD $i set-focused-tags $tags

    # $MOD+Shift+[0-9] to tag focused view with tag [0-9]
    riverctl map normal $MOD+Shift $i set-view-tags $tags

    # $MOD+Ctrl+[0-9] to toggle focus of tag [0-9]
    riverctl map normal $MOD+Control $i toggle-focused-tags $tags

    # $MOD+Shift+Ctrl+[0-9] to toggle tag [0-9] of focused view
    riverctl map normal $MOD+Shift+Control $i toggle-view-tags $tags
end


### window mode
# $MOD+F to toggle fullscreen
riverctl map normal $MOD F toggle-fullscreen



### navigation
# $MOD+{Up,Right,Down,Left} to change layout orientation
riverctl map normal $MOD Up    send-layout-cmd rivertile "main-location top"
riverctl map normal $MOD Right send-layout-cmd rivertile "main-location right"
riverctl map normal $MOD Down  send-layout-cmd rivertile "main-location bottom"
riverctl map normal $MOD Left  send-layout-cmd rivertile "main-location left"







# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked;
    # Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
    riverctl map $mode None XF86AudioRaiseVolume  spawn 'pamixer -i 5'
    riverctl map $mode None XF86AudioLowerVolume  spawn 'pamixer -d 5'
    riverctl map $mode None XF86AudioMute         spawn 'pamixer --toggle-mute'

    # Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
    riverctl map $mode None XF86AudioMedia spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPlay  spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPrev  spawn 'playerctl previous'
    riverctl map $mode None XF86AudioNext  spawn 'playerctl next'

    # Control screen backlight brightness with light (https://github.com/haikarainen/light)
    riverctl map $mode None XF86MonBrightnessUp   spawn 'light -A 5'
    riverctl map $mode None XF86MonBrightnessDown spawn 'light -U 5'
end
