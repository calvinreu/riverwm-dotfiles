#!/bin/fish
set COLORSCEME "unset"

# Select sceme
switch $COLORSCEME
    case "frappe"
        # --> special
        set foreground c6d0f5
        set selection 414559
        # --> palette
        set teal 81c8be
        set flamingo eebebe
        set mauve ca9ee6
        set pink f4b8e4
        set red e78284
        set peach ef9f76
        set green a6d189
        set yellow e5c890
        set blue 8caaee
        set gray 737994
    case "latte"
        # --> special
        set  foreground 4c4f69
        set  selection ccd0da
        # --> palette
        set  teal 179299
        set  flamingo dd7878
        set  mauve 8839ef
        set  pink ea76cb
        set  red d20f39
        set  peach fe640b
        set  green 40a02b
        set  yellow df8e1d
        set  blue 1e66f5
        set  gray 9ca0b0
    case "macchiato"
        # --> special
        set foreground cad3f5
        set selection 363a4f
        # --> palette
        set teal 8bd5ca
        set flamingo f0c6c6
        set mauve c6a0f6
        set pink f5bde6
        set red ed8796
        set peach f5a97f
        set green a6da95
        set yellow eed49f
        set blue 8aadf4
        set gray 6e738d
    case "mocha"
        # --> special
        set foreground cdd6f4
        set selection 313244
        # --> palette
        set teal 94e2d5
        set flamingo f2cdcd
        set mauve cba6f7
        set pink f5c2e7
        set red f38ba8
        set peach fab387
        set green a6e3a1
        set yellow f9e2af
        set blue 89b4fa
        set gray 6c7086
    case '*'
        echo "unknown colorsceme"
end

# Syntax Highlighting
set -g fish_color_normal $foreground
set -g fish_color_command $blue
set -g fish_color_param $flamingo
set -g fish_color_keyword $red
set -g fish_color_quote $green
set -g fish_color_redirection $pink
set -g fish_color_end $peach
set -g fish_color_error $red
set -g fish_color_gray $gray
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $pink
set -g fish_color_escape $flamingo
set -g fish_color_autosuggestion $gray
set -g fish_color_cancel $red

# Prompt
set -g fish_color_cwd $yellow
set -g fish_color_user $teal
set -g fish_color_host $blue

# Completion Pager
set -g fish_pager_color_progress $gray
set -g fish_pager_color_prefix $pink
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $gray

# River Colors
set -g river_color_foreground $mauve
set -g river_color_background $flamingo