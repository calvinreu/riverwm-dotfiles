#!/usr/bin/env bash

#load config
CONFIG="$(cat ~/.config/wallpaper/wallpaper.conf | grep -v "#")"

WALLPAPERS_PATH=$(echo "$CONFIG" | grep WALLPAPERS_PATH)
WALLPAPERS_PATH="${WALLPAPERS_PATH#*:}"
SCALLING_MODE=$(echo "$CONFIG" | grep SCALLING_MODE)
SCALLING_MODE="${SCALLING_MODE#*:}"
BACKGROUND_TYPE="$(echo "$CONFIG" | grep BACKGROUND_TYPE)"
BACKGROUND_TYPE="${BACKGROUND_TYPE#*:}"
SLIDE_DELAY=$(echo "$CONFIG" | grep SLIDE_DELAY)
SLIDE_DELAY=${SLIDE_DELAY#*:}

slideshow_w () {
  while ! (($RIVERWM_RESET)); do
    swaybg -i "$(find "$WALLPAPERS_PATH" -type f |  shuf -n 1)" -m $SCALLING_MODE
    sleep $SLIDE_DELAY
  done
}

static_w () {
  swaybg -i "$(find "$WALLPAPERS_PATH" -type f |  shuf -n 1)" -m $SCALLING_MODE
}

## BACKGROUND_TYPES ##
case "$BACKGROUND_TYPE" in
"slideshow") 
  slideshow_w;;
"static") 
  static_w;;
*) 
  echo "unknown wallpaper type change wallpaper.conf";;
esac